import Gauge from './Gauge';

function TraitPanel({
  value,
  type,
  name,
  onRemove,
}: {
  name: string;
  type: 'positive' | 'negative';
  value: number;
  onRemove?: () => void;
}) {
  const capitalizedName = `${name.charAt(0).toUpperCase()}${name.slice(1)}`;

  return (
    <article className="trait-panel">
      <style jsx>{`
        .trait-panel {
          position: relative;
        }

        h4 {
          text-align: center;
        }

        button {
          position: absolute;
          top: 0;
          right: 0;
        }
      `}</style>

      <Gauge type={type} value={value} />

      <h4>
        {capitalizedName}: {value}
      </h4>

      <button onClick={onRemove}>×</button>
    </article>
  );
}

export default TraitPanel;
