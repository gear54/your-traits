import { groupBy, sumBy } from 'lodash';

function SuccessLevel({ parts }: { parts: { type: 'positive' | 'negative'; value: number }[] }) {
  const keyed = groupBy(parts, 'type');
  const positive = sumBy(keyed.positive, 'value');
  const negative = sumBy(keyed.negative, 'value');
  const difference = positive - negative;
  const isSuccessful = difference > 0;
  const absolute = Math.abs(difference);

  return (
    <>
      <style jsx>{`
        .bad {
          color: red;
        }

        .good {
          color: green;
        }
      `}</style>
      {isSuccessful ? (
        <h3 className="good">Уровень успеха: {absolute}</h3>
      ) : (
        <h3 className="bad">Уровень дна: {absolute}</h3>
      )}
    </>
  );
}

export default SuccessLevel;
