import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Head from 'next/head';

import TraitPanel from '@/components/TraitPanel';
import SuccessLevel from '@/components/SuccessLevel';

import useQueryTraits from '@/helpers/useQueryTraits';

const cleanTraitModel = { name: '', value: '', isNegative: true };

function Index() {
  const router = useRouter();
  const [traits, setTraits, traitsToQuery] = useQueryTraits();
  const [newTraitModel, setNewTraitModel] = useState(cleanTraitModel);

  useEffect(() => {
    const handler = () => setNewTraitModel(cleanTraitModel);

    router.events.on('routeChangeStart', handler);

    return () => router.events.off('routeChangeStart', handler);
  }, [router]);

  const newTrait: typeof traits[0] = {
    name: newTraitModel.name.trim(),
    value: parseFloat(newTraitModel.value),
    type: newTraitModel.isNegative ? 'negative' : 'positive',
  };

  const isNewTraitValid =
    newTrait.name.length > 0 &&
    newTrait.value > 0 &&
    !traits.some(({ name }) => name.toLowerCase() === newTrait.name.toLowerCase());

  return (
    <>
      <style jsx>{`
        .negative-label {
          user-select: none;
        }

        .traits {
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          gap: 16px;
        }
      `}</style>
      <Head>
        <title>Your Traits</title>
      </Head>
      <main>
        <form
          onSubmit={(event) => {
            event.preventDefault();

            if (isNewTraitValid) {
              setTraits([...traits, newTrait]);
            }
          }}
          onKeyUp={(event) => {
            if (event.key === 'Enter') {
              event.currentTarget.requestSubmit();
            }
          }}
        >
          <input
            type="text"
            placeholder="Лень"
            value={newTraitModel.name}
            onChange={({ target: { value } }) => setNewTraitModel({ ...newTraitModel, name: value })}
          />
          <input
            type="number"
            step="0.001"
            placeholder="5"
            value={newTraitModel.value}
            onChange={({ target: { value } }) => setNewTraitModel({ ...newTraitModel, value: value })}
          />

          <label className="negative-label">
            <input
              type="checkbox"
              checked={newTraitModel.isNegative}
              onChange={({ target: { checked } }) => setNewTraitModel({ ...newTraitModel, isNegative: checked })}
            />
            Негативное
          </label>
          {isNewTraitValid && <Link href={{ query: traitsToQuery([...traits, newTrait]) }}>Добавить</Link>}
        </form>

        <section className="traits">
          {traits.map(({ name, type, value }, index) => (
            <TraitPanel
              key={name}
              name={name}
              type={type}
              value={value}
              onRemove={() => setTraits(traits.filter((_trait, traitIndex) => traitIndex !== index))}
            />
          ))}
        </section>

        <SuccessLevel parts={traits} />
      </main>
    </>
  );
}

export default Index;
