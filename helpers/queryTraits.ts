import { map, uniqBy } from 'lodash';

import { ParsedUrlQuery, ParsedUrlQueryInput } from 'querystring';

export type Trait = {
  type: 'positive' | 'negative';
  name: string;
  value: number;
};

function traitsToQuery(traits: Trait[]): ParsedUrlQueryInput {
  const traitMap = traits.map(({ type, name, value }): [string, string] => [
    `${type === 'negative' ? '-' : ''}${name}`,
    String(value),
  ]);

  return Object.fromEntries(traitMap);
}

function queryToTraits(query: ParsedUrlQuery): Trait[] {
  const traits = map(query, (queryValue, signedName) => {
    if (!queryValue || Array.isArray(queryValue)) {
      return undefined;
    }

    const value = parseFloat(queryValue);

    if (Number.isNaN(value)) {
      return undefined;
    }

    const isNegative = signedName.startsWith('-');
    const name = signedName.slice(isNegative ? 1 : 0);

    if (!name) {
      return undefined;
    }

    return {
      type: isNegative ? 'negative' : 'positive',
      name,
      value: Math.abs(value),
    };
  }).filter((v): v is Trait => Boolean(v));

  return uniqBy(traits, 'name');
}

export { traitsToQuery, queryToTraits };
