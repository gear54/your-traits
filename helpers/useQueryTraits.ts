import { ParsedUrlQueryInput } from 'querystring';

import { useRouter } from 'next/router';
import { isEqual } from 'lodash-es';

import { traitsToQuery, queryToTraits, Trait } from '@/helpers/queryTraits';

function useQueryTraits(): [Trait[], (traits: Trait[]) => Promise<boolean>, (traits: Trait[]) => ParsedUrlQueryInput] {
  const router = useRouter();
  const traits = queryToTraits(router.query);
  const traitsQuery = traitsToQuery(traits);

  const setTraits = (traits: Trait[]) => router.push({ query: traitsToQuery(traits) });

  // Normalize URL
  if (!isEqual(traitsToQuery(traits), router.query)) {
    router.replace({ query: traitsQuery });
  }

  return [traits, setTraits, traitsToQuery];
}

export default useQueryTraits;
